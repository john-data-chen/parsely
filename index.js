var Botkit = require('botkit')
var request = require('request')

const CLIENT_ID = '302640019077.484860880420'
// const CLIENT_ID = '140588235348.485787908418'
const CLIENT_SECRET = '7ab2489de2d3259f6f1f03371e2a863c'
// const CLIENT_SECRET = 'da5a161e08d52b70cafad888d112d617'
const PORT = 8765
const VERIFICATION_TOKEN = 'ehPxZHtoZBT9WVMP8DbUqGWG'
// const VERIFICATION_TOKEN = 'VNyjWWWvW57tBzKYcN4Wy4GK'
const HELP_SPEECH = '需要幫忙嗎？\n/twply posts, 15m\n這指令會給你15分鐘內最熱門文章\n可以查看的選項 posts / referrers / tags / sections\n時間可以是 15m  / 12h / 3d (15分鐘 / 12小時 / 3天 前到現在)'
const NO_RESULTS_SPEECH = '您的搜尋條件目前沒有結果，請更換其他選項'
const API_KEY = 'appledaily.com.tw'
const API_SECRET = 'iKR2eoYlM3Kq9odv905nAOtXXyrFM701mUHMqGCzZVI'

var config = {
  json_file_store: './db_slackbutton_slash_command/'
}

var controller = Botkit.slackbot(config).configureSlackApp(
  {
    clientId: CLIENT_ID,
    clientSecret: CLIENT_SECRET,
    scopes: ['commands']
  }
)

controller.setupWebserver(PORT, function (err, webserver) {
  controller.createWebhookEndpoints(controller.webserver)

  controller.createOauthEndpoints(controller.webserver, function (err, req, res) {
    if (err) {
      console.log('controller.err:')
      res.status(500).send('ERROR: ' + err)
    } else {
      res.send('Success!')
    }
  })
})

controller.on('slash_command', function (slashCommand, message) {
  // token incorrect
  if (message.token !== VERIFICATION_TOKEN) {
    console.log('VERIFICARION_TOKEN incorrect!')
    return // just ignore it.
  }
  if (message.command === '/twply') {
    var parameters = message.text.split(', ')
    // fix parameters will add twice
    var PARSELY_URL = 'https://api.parsely.com/v2/analytics/'
    // console.log(parameters)

    switch (parameters[0]) {
      case 'posts':
      case 'referrers':
      case 'sections':
      case 'tags':
        PARSELY_URL += parameters[0] + '?apikey=' + API_KEY + '&secret=' + API_SECRET
        if (typeof (parameters[1]) == 'string')
          PARSELY_URL += '&period_start=' + parameters[1]
        else
          PARSELY_URL += '&period_start=1h'
        // console.log(PARSELY_URL)
        request.get(PARSELY_URL, function (error, response, body) {
          // error handle
          if (error) {
            console.log(error)
            console.log('url: ', PARSELY_URL)
            return
          }
          if (response.statusCode == 204 || response.statusCode >= 400) {
            error = 'app.post/search response.statusCode: ' + response.statusCode
            console.log(error)
            console.log(JSON.stringify(response, null, 2))
            return
          }
          // console.log(body)
          // string to JSON
          body = JSON.parse(body)
          // error handle if body has no data
          if (!body.data || body.data.length <= 0) {
            slashCommand.replyPrivate(message, NO_RESULTS_SPEECH)
            return
          }
          switch (parameters[0]) {
            case 'posts':
              var reply = {
                'attachments': []
              }
              for (let index = 0; index < body.data.length; index++) {
                var element = body.data[index]
                var item = {
                  'title': element.title,
                  'title_link': element.url,
                  'text': '點擊數：' + element._hits
                }
                reply.attachments.push(item)
                if (index == (body.data.length - 1)) {
                  slashCommand.replyPublic(message, reply)
                  return
                }
              }
              break
            case 'referrers':
              var reply = ''
              for (let index = 0; index < body.data.length; index++) {
                var element = body.data[index]
                reply += '類別：' + element.category + '\n名稱：' + element.title + '\n點擊數：' + element._hits + '\n'
                if (index == (body.data.length - 1)) {
                  slashCommand.replyPublic(message, reply)
                  return
                }
              }
              break
            case 'sections':
              var reply = ''
              for (let index = 0; index < body.data.length; index++) {
                var element = body.data[index]
                reply += element.section + ', 點擊數：' + element._hits + '\n'
                if (index == (body.data.length - 1)) {
                  slashCommand.replyPublic(message, reply)
                  return
                }
              }
              break
            case 'tags':
              var reply = ''
              for (let index = 0; index < body.data.length; index++) {
                var element = body.data[index]
                reply += element.tag + ', 點擊數：' + element._hits + '\n'
                if (index == (body.data.length - 1)) {
                  slashCommand.replyPublic(message, reply)
                  return
                }
              }
              break
            // error handle of no results
            default:
              slashCommand.replyPrivate(message, NO_RESULTS_SPEECH)
              return
          }
        })
        break
      // error handle of incorrect command
      default:
        slashCommand.replyPrivate(message, HELP_SPEECH)
        return
    }
  }
})
